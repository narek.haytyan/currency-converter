import React from 'react';
import PropTypes from 'prop-types';

import {
    Input,
    MenuItem,
    Select,
} from '@material-ui/core';

import styles from './CurrencyInput.style';

function CurrencyInput(props) {
    const {
        amount,
        currencyList,
        selectedCurrency,
        disabled,
        onAmountChange,
        onCurrencyChange,
    } = props;

    const classes = styles();

    return (
        <div className={classes.root}>
            <div>
                <Input
                    className={classes.currencyInput}
                    type="number"
                    inputProps={{
                        min: 0,
                    }}
                    value={amount}
                    disabled={disabled}
                    onChange={onAmountChange}
                />
            </div>
            <Select
                value={selectedCurrency}
                disableUnderline
                onChange={onCurrencyChange}
            >
                {currencyList.map(cur => (
                    <MenuItem
                        key={cur}
                        value={cur}
                    >
                        {cur}
                    </MenuItem>
                ))}
            </Select>
        </div>
    );
}

CurrencyInput.propTypes = {
    amount: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]).isRequired,
    currencyList: PropTypes.arrayOf(PropTypes.string).isRequired,
    disabled: PropTypes.bool,
    selectedCurrency: PropTypes.string.isRequired,
    onCurrencyChange: PropTypes.func.isRequired,
};

CurrencyInput.defaultProps = {
    amount: '',
    disabled: false,
    onCurrencyChange: () => {},
    onAmountChange: () => {},
};

export default CurrencyInput;
