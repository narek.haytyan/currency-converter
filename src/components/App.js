import React from 'react';
import axios from 'axios';
import { formatDistance } from 'date-fns';

import {
    IconButton,
    CssBaseline,
    Fab,
    Paper,
    Tooltip,
    Typography,
} from '@material-ui/core';

import {
    Cached,
    Info,
    SwapVertRounded,
} from '@material-ui/icons';

import CurrencyInput from './CurrencyInput';

import styles from './App.styles';

const API_ORIGIN = 'https://api.exchangeratesapi.io';

function App() {
    const classes = styles();

    const [latestRates, setLatestRates] = React.useState({});
    const [currencyList, setCurrencyList] = React.useState(['EUR', 'USD']);
    const [shouldUpdateRates, setShouldUpdateRates] = React.useState(true);
    const [isUpdatingRates, setIsUpdatingRates] = React.useState(false);
    const [currencyFrom, setCurrencyFrom] = React.useState('EUR');
    const [amountFrom, setAmountFrom] = React.useState(0);
    const [currencyTo, setCurrencyTo] = React.useState('USD');
    const [amountTo, setAmountTo] = React.useState(0);
    const [lastUpdated, setLastUpdated] = React.useState(null);

    const updateRates = React.useCallback((base = currencyFrom) => {
        // if there is a pending request then do nothing
        if (isUpdatingRates) {
            return;
        }

        setIsUpdatingRates(true);

        // requesting latest rates
        return axios.get(`${API_ORIGIN}/latest?base=${base}`)
            .then(result => {
                const rates = result.data.rates;
                const currencyList = Object.keys(rates);
                if (currencyList.includes(base) === false) {
                    currencyList.push(base);
                    rates[base] = 1;
                }
                setLatestRates(rates);
                setCurrencyList(currencyList.sort());
                setIsUpdatingRates(false);
                setLastUpdated(Date.now());

                return rates;
            });
    }, [isUpdatingRates, currencyFrom]);

    React.useEffect(() => {
        if (shouldUpdateRates) {
            setShouldUpdateRates(false);

            updateRates();
        }
    }, [shouldUpdateRates, updateRates]);

    function isValidValue(value) {
        const parsedValue = Number.parseFloat(value);
        const isInvalid = Number.isNaN(parsedValue) || parsedValue < 0;

        return isInvalid === false;
    }

    async function handleCurrencyFromChange(e) {
        const newCurrency = e.target.value;
        setCurrencyFrom(newCurrency);

        const newRates = await updateRates(newCurrency);
        setAmountTo(amountFrom * newRates[currencyTo]);
    }

    function handleAmountFromChange(e) {
        const value = e.target.value;

        if (value.length === 0) {
            setAmountFrom('');
            setAmountTo(0);
        } else if (isValidValue(value)) {
            const newAmount = Number.parseFloat(value);
            setAmountFrom(newAmount);
            setAmountTo(newAmount * latestRates[currencyTo]);
        } else {
            setAmountFrom(0);
            setAmountTo(0);
        }
    }

    function handleCurrencyToChange(e) {
        const newCurrency = e.target.value;
        setCurrencyTo(newCurrency);

        setAmountTo(amountFrom * latestRates[newCurrency]);
    }

    function handleAmountToChange(e) {
        const value = e.target.value;

        if (value.length === 0) {
            setAmountTo('');
            setAmountFrom(0);
        } else if (isValidValue(value)) {
            const newAmount = Number.parseFloat(value);
            setAmountTo(newAmount);
            setAmountFrom(newAmount / latestRates[currencyTo]);
        } else {
            setAmountTo(0);
        }
    }

    async function handleBaseSwitch() {
        const oldBase = currencyFrom;
        const newBase = currencyTo;
        setCurrencyFrom(newBase);
        setCurrencyTo(oldBase);

        const rates = await updateRates(newBase);
        setAmountTo(amountFrom * rates[oldBase]);
    }

    function handleUpdateClick() {
        updateRates();
        setAmountTo(amountFrom * latestRates[currencyTo]);
    }

    return (
        <>
            <CssBaseline />

            <Paper elevation={3} className={classes.paper}>
                <div className={classes.info}>
                    <Info fontSize="small" />
                    <Typography variant="caption" className={classes.infoText}>
                        Rates updated: {lastUpdated ? formatDistance(lastUpdated, Date.now(), { addSuffix: true }) : 'never'}
                    </Typography>
                    <Tooltip title="Click to update rates">
                        <IconButton onClick={handleUpdateClick}>
                            <Cached fontSize="small" />
                        </IconButton>
                    </Tooltip>
                </div>
                <div className={`${classes.paperHalf} ${classes.topHalf}`}>
                    <CurrencyInput
                        amount={amountFrom}
                        currencyList={currencyList.filter(c => c !== currencyTo)}
                        selectedCurrency={currencyFrom}
                        disabled={Object.keys(latestRates).length === 0}
                        onCurrencyChange={handleCurrencyFromChange}
                        onAmountChange={handleAmountFromChange}
                    />
                </div>
                <Fab className={classes.fab} size="small" onClick={handleBaseSwitch}>
                    <SwapVertRounded />
                </Fab>
                <div className={`${classes.paperHalf} ${classes.bottomHalf}`}>
                    <CurrencyInput
                        amount={amountTo}
                        currencyList={currencyList.filter(c => c !== currencyFrom)}
                        selectedCurrency={currencyTo}
                        disabled={Object.keys(latestRates).length === 0}
                        onCurrencyChange={handleCurrencyToChange}
                        onAmountChange={handleAmountToChange}
                    />
                </div>
            </Paper>
        </>
    );
}

export default App;
