import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ spacing }) => {
    return {
        root: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        },
        currencyInput: {
            fontSize: '2rem',
            marginRight: spacing(2),
        },
        errorHelperText: {
            color: '#ff0000',
        },
    };
}, {
    name: 'CurrencyInput',
});
