import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ spacing }) => {
    return {
        paper: {
            width: '100vw',
            height: '100vh',
            maxWidth: '300px',
            maxHeight: '400px',

            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'stretch',
            overflow: 'hidden',
        },
        paperHalf: {
            display: 'flex',
            flex: 1,
            padding: spacing(2),
        },
        topHalf: {
            background: 'linear-gradient(to bottom left, rgba(175,255,76,1) 0%, rgba(175,255,165,1) 100%)',
        },
        bottomHalf: {
            background: '#ffffff',
        },
        fab: {
            position: 'absolute',
            right: spacing(3),
            top: '50%',
            transform: 'translateY(-50%)',
        },
        info: {
            position: 'absolute',
            top: spacing(1),
            left: spacing(1),
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        },
        infoText: {
            marginLeft: spacing(1),
        },
    };
}, {
    name: 'App',
});
